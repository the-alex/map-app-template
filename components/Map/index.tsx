import React, { useRef, useEffect, useState } from "react";
import maplibregl from "maplibre-gl";

interface mapProps {
  selectedLayer: string;
}

export default function Map(props: mapProps) {

  console.log(props);

  const mapContainer = useRef(null);
  const map = useRef<maplibregl.Map | null>(null);
  const [lng] = useState(-72.209);
  const [lat] = useState(44.935);
  const [zoom] = useState(15);
  const [API_KEY] = useState("Hj56U83TJbxnR02sfWnX");


  useEffect(() => {
    if (map.current) return;
    map.current = new maplibregl.Map({
      container: mapContainer.current!,
      style: `https://api.maptiler.com/maps/dataviz/style.json?key=${API_KEY}`,
      center: [lng, lat],
      zoom: zoom,
    });

    if (!map.current) return;
    map.current.on("load", () => {
      if (map.current) {
        map.current.addSource("zipcodes", {
          type: "geojson",
          data: "/data/vt_zipcodes.geojson",
        });

        map.current.addLayer({
          id: "zipcodes",
          type: "fill",
          source: "zipcodes",
          paint: {
            "fill-color": "#a288b8",
            "fill-opacity": 0.1,
            "fill-outline-color": "#000000",
          },
        });

        // map.current.setLayoutProperty("zipcodes", "visibility", "visible");
      }
    });
  });


  return (
    <div className="relative flex grow">
      <div ref={mapContainer} className="flex grow" />
    </div>
  );
}

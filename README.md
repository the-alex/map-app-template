Map Application Template
========================

## Motivation

I've got some decent development experience under me, mostly in service of taking geospaitial data science projects and turning them into products. Each time, I've learned a little bit more about developing these kinds of applications, and I'm getting a little sick of starting from scratch each time. The motivation for this repo is to give map application development a decent starting place.

- Static data
- Simple geospatial exploration
- Simple interactions
- Configured CI/CD

Eventually, I might support things like pipelines for generating and styling your own vector tiles so you can ditch maptiler and mapbox as well, getting off the managed services.


This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

## Getting Started

First, run the development server:

```bash
npm run dev
# or
yarn dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

You can start editing the page by modifying `pages/index.tsx`. The page auto-updates as you edit the file.

[API routes](https://nextjs.org/docs/api-routes/introduction) can be accessed on [http://localhost:3000/api/hello](http://localhost:3000/api/hello). This endpoint can be edited in `pages/api/hello.ts`.

The `pages/api` directory is mapped to `/api/*`. Files in this directory are treated as [API routes](https://nextjs.org/docs/api-routes/introduction) instead of React pages.


import path from 'path';
import { promises as fs } from 'fs';

export default async function handler(req, res) {
    const jsonDir = path.join(process.cwd(), 'json')
    const contents = await fs.readFile(jsonDir + '/meters.geojson', 'utf8')
    res.status(200).json(contents)
}

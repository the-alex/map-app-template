import Head from "next/head";
import Map from "../components/Map";
import Table from "../components/Table";
import Sidebar from "../components/Sidebar";

export default function Home() {
  return (
    <div className="container">
      <Head>
        <title>Map Template!</title>
        <meta name="description" content="A decent place to start for any map project." />
        <link rel="icon" href="/favicon.ico" />
        <link
          href="https://unpkg.com/maplibre-gl@2.4.0/dist/maplibre-gl.css"
          rel="stylesheet"
        />
      </Head>

      <div className="h-screen w-screen flex">
        <Sidebar />
        <div className="flex flex-col w-full">
          <Map selectedLayer={'zipcodes'} />
        </div>
      </div>

    </div>
  );
}
